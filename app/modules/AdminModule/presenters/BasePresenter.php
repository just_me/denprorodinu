<?php

namespace App\AdminModule\Presenters;

use Nette;
use Nette\Utils\Finder;
use Nette\Http\Response;

/*
 * Pharo
 */

/**
 * Description of BasePresenter
 *
 * @author hippo
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

    public $db;
    public $httpResponse;
    public $httpRequest;

    public function __construct(Nette\Database\Context $database)
    {
        $this->db = $database;
        parent::__construct();
        $httpResponse = new Response();
        $httpResponse->addHeader('Pragma', 'no-cache');
        $this->httpResponse = $httpResponse;
    }

    public function startup()
    {
        parent::startup();
        $this->checkLogin();
    }

    protected function checkLogin()
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
    }

    protected function bootstrapize_controls(&$container)
    {
        foreach ($container->getComponents() as $control) {

            if ($control instanceof Forms\Container) {
                $this->bootstrapize_controls($control);
                continue;
            }

            if ($control instanceof Controls\Button) {
                $control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-default');
                $usedPrimary = TRUE;
            } elseif ($control instanceof Controls\TextBase || $control instanceof Controls\SelectBox || $control instanceof Controls\MultiSelectBox) {
                $control->getControlPrototype()->addClass('form-control');
            } elseif ($control instanceof Controls\Checkbox || $control instanceof Controls\CheckboxList || $control instanceof Controls\RadioList) {
                $control->getSeparatorPrototype()
                        ->setName('div')
                        ->addClass($control->getControlPrototype()->type);
            }

            if ($control instanceof Controls\Checkbox) {
                $control->getLabelPrototype()->addClass('checkbox');
            } elseif ($control instanceof Controls\CheckboxList || $control instanceof Controls\RadioList) {
                $control->getLabelPrototype()->addClass('label_check');
            }
        }
    }

    protected function bootstrapize(Nette\Application\UI\Form &$form, $orientation = 'form-horizontal')
    {
        $renderer = $form->getRenderer();
        $renderer->wrappers['controls']['container'] = NULL;
        $renderer->wrappers['pair']['container'] = 'div class=form-group';
        $renderer->wrappers['pair']['.error'] = 'has-error';
        $renderer->wrappers['control']['container'] = 'div class=col-lg-10';
        $renderer->wrappers['label']['container'] = 'div class="col-lg-2 col-sm-2 control-label"';
        $renderer->wrappers['control']['description'] = 'p class=help-block';
        $renderer->wrappers['control']['errorcontainer'] = 'p class=help-block';

        $form->getElementPrototype()->class($orientation);

        $this->bootstrapize_controls($form);
    }

}
