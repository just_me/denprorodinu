<?php

/*
 * Pharo
 */

namespace App\AdminModule\Presenters;

use Nette;
use Nette\Application\UI\Form;

/**
 * Description of SignPresenter
 *
 * @author hippo
 */
class SignPresenter extends Nette\Application\UI\Presenter
{

    /**
     * Sign-in form factory.
     *
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignInForm()
    {
        $form = new Form();

        $form->addText('username', 'Přihlašovací jméno:')
                        ->setRequired('Vyplňte přihlašovací jméno.')
                        ->getControlPrototype()->class = 'form-control';

        $form->addPassword('password', 'Heslo:')
                        ->setRequired('Vyplňte heslo.')
                        ->getControlPrototype()->class = 'form-control';



        $form->addSubmit('send', 'Přihlásit'
                )->getControlPrototype()->class = 'btn btn-primary pull-right';
        $form->addHidden('firsttime', '');
        $form->addHidden('email', '');

        // call method signInFormSucceeded() on success
        $form->onSuccess[] = [$this, 'signInFormSucceeded'];

        $renderer = $form->getRenderer();
        $renderer->wrappers['controls']['container'] = null;
        $renderer->wrappers['pair']['container'] = 'div class="form-group"';
        $renderer->wrappers['label']['container'] = null;
        $renderer->wrappers['control']['container'] = null;

        $form->getElementPrototype()->class('sky-form boxed');
        return $form;
    }

    public function signInFormSucceeded($form)
    {
        $values = $form->getValues();
        $this->getUser()->setExpiration('14 days', FALSE);
        try {
            $this->getUser()->login($values->username, $values->password);
            $this->getUser()
                    ->getStorage()
                    ->setNamespace('admin');
            $this->flashMessage('Byl jste úspěšně přihlášen!', 'success');
            $this->redirect('Dashboard:');
        } catch (Nette\Security\AuthenticationException $e) {
            $this->flashMessage('Zadali jste chybné přihlašovací údaje!', 'danger');
        }
    }

}
