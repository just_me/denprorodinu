<?php

/*
 * Pharo
 */

namespace App\AdminModule\Presenters;

use Nette;
use Nette\Application\UI\Form;

/**
 * Description of WebcontentPresenter
 *
 * @author hippo
 */
class WebcontentPresenter extends BasePresenter
{

    private $eid;
    protected $eresult;

    public function renderDefault()
    {
        $pages = $this->db->table('texty')
                ->where('s_version', 1)
                ->fetchAll();
        $this->template->pages = $pages;
    }

    public function actionEdit($id)
    {

        $this->eid = $id;
        $result = $this->db->table('texty')->where(['id' => $id])->fetch();
        $this->eresult = $result;
    }

    public function createComponentEditPageForm()
    {

        $form = new Form();
        $form->addHidden('id', $this->eid);
        $form->addHidden('s_name', $this->eresult->s_name);
        $form->addHidden('presenter', $this->eresult->presenter);
        $form->addHidden('s_version', $this->eresult->s_version);
        $form->addText('s_name_show', 'KOD')->setAttribute('class', 'form-control')->setDisabled(TRUE);
        $form->addText('s_updated', 'Posledni zmena')->setAttribute('class', 'form-control')->setDisabled(true);
        $form->addText('s_description', 'Popis')->setAttribute('class', 'form-control');
        $form->addTextArea('s_content', 'Text')->setAttribute('class', 'form-control')->setAttribute('style', "height: 600px; width: 100%;");
        $form->addSubmit('save', 'Uložit');
        $defaults = [
            's_name_show'   => $this->eresult->s_name,
            's_updated'     => $this->eresult->s_updated->format('j.m. Y H:i:s'),
            's_description' => $this->eresult->s_description,
            's_content'     => $this->eresult->s_content
        ];

        $form->setDefaults($defaults);
        $form->onSuccess[] = [$this, 'editFormSucceeded'];
        $form->getElementPrototype()->class('form-horizontal validate');
        $form->getElementPrototype()->addAttributes(
                ['data-toastr-position' => 'top-right']
        );
        $this->bootstrapize($form);
        return $form;
    }

    public function editFormSucceeded(Form $form)
    {
        try {
            $values = $form->getValues();
            $new_version = $this->db->table('texty')
                    ->select('MAX(s_version)+1 max')
                    ->where('presenter', $values['presenter'])
                    ->where('s_name', $values['s_name'])
                    ->fetch();

            $this->db->table('texty')
                    ->where('id', $values['id'])
                    ->update(['s_version' => $new_version->max]);
            $data = [
                'presenter'     => $values['presenter'],
                's_name'        => $values['s_name'],
                's_version'     => 1,
                's_description' => $values['s_description'],
                's_content'     => $values['s_content'],
                's_updated'     => new Nette\Database\SqlLiteral('NOW()')
            ];
            $this->db->table('texty')->insert($data);
            $this->flashMessage('Zaznam ' . $data['s_version'] . ' byl uspesne ulozen', 'success');
            $this->redirect('default');
        } catch (Nette\InvalidArgumentException $e) {
            $this->flashMessage('Zaznam se nepodarilo ulozit', 'error');
        } catch (Nette\Database\DriverException $e) {
            $this->flashMessage('Zaznam se nepodarilo ulozit', 'error');
        }
    }

    public function countOldVersion($presenter, $s_name)
    {
        return $this->db->table('texty')
                        ->where(['presenter' => $presenter])
                        ->where(['s_name' => $s_name])
                        ->where('s_version>1')
                        ->count();
    }

    public function renderHistory($s_presenter, $s_name)
    {

        $pages = $this->db->table('texty')
                ->where('presenter', $s_presenter)
                ->where('s_name', $s_name)
                ->where('s_version>1')
                ->fetchAll();
        $this->template->pages = $pages;

        
    }

}
