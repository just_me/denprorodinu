<?php

namespace App\FrontModule\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Utils\Validators;
use Nette\Utils\DateTime;
use Common\Ip;

class HomepagePresenter extends BasePresenter
{

    public function renderDefault()
    {
        $this->template->footer_partners = $this->getPartners(true);
        $this->template->main_partners = $this->getMainPartners();

        $interval = $this->current_date->diff($this->t_date);
        $this->template->interval = $interval->format('%a') + 2;
        //$this->flashMessage('Hotovo','alert-info');
        
        $users_result = $this->db->table('prihlaska')->count();
        $this->template->users_result = $users_result+2;
    }

    protected function createComponentAddappForm()
    {

        $form = new Form();
        $a = 0;
        $form->elementPrototype->addAttributes(array('class' => 'register-form'));
        $form->addText('name_lastname', 'Vaše jméno a příjmení')
                ->setAttribute('class', 'form-control input-lg')
                ->setAttribute('placeholder', 'Vaše jméno a příjmení')
                ->setAttribute('tabindex', $a++)
                ->setRequired(true);
        $form->addEmail('email', 'Emailová adresa')
                ->setAttribute('class', 'form-control input-lg')
                ->setAttribute('placeholder', 'Emailová adresa')
                ->setAttribute('tabindex', $a++)
                ->setRequired(true);
        $form->addText('child_count', 'Počet dětí')
                ->setAttribute('class', 'form-control input-lg')
                ->setAttribute('placeholder', 'Počet dětí')
                ->setAttribute('tabindex', $a++);
        $form->addText('message', 'Nějaký vzkaz ?')
                ->setAttribute('class', 'form-control input-lg')
                ->setAttribute('placeholder', 'Nějaký vzkaz ?')
                ->setAttribute('tabindex', $a++);
        $form->addSubmit('submit', 'Přihlásit se')
                ->setAttribute('class', 'btn btn-theme btn-block btn-lg chmnow')
                ->setAttribute('tabindex', $a++);
        $form->addHidden('c_type', 'user');
        $form->onSuccess[] = [
            $this,
            'cAppSuccessed'
        ];
        return $form;
    }

    public function cAppSuccessed(Form $form)
    {
        $values = $form->getValues();

        if (
                empty($values->name_lastname) ||
                empty($values->email) /* ||
          empty($values->child_count) */
        ) {
            $this->flashMessage('Vyplňte prosím všechny povinně údaje.', 'error');
            $form->addError('Vyplňte prosím všechny povinně údaje.');
            $this->redirect('default');
        }
        if (Validators::isEmail($values->email) === false) {
            $this->flashMessage('Email musí být ve správném tvaru.', 'error');
            $form->addError('Email musí být ve správném tvaru.');
            $this->redirect('default');
        }
        //$child_count = (int) $values->child_count;
        $dat = new DateTime();
        $subject = 'Dotaz z www.denprorodinu.eu';
        $mail = new Message();
        $mail->setEncoding('UTF-8');
        $mail->setFrom('web@erneks.cz');
        //$mail->addTo('hippo@localhost');
        $mail->addTo('info@denprorodinu.eu');
        $mail->addCc('hippo@network.cz');
        $mail->setSubject($subject);
        $mail->setBody('
|------------------------------------------------------------------------------|
| Email Prihlaska denprorodinu.eu
|------------------------------------------------------------------------------|
| OD: ' . $values->name_lastname . ' 
| EMAIL: ' . $values->email . ' 
|------------------------------------------------------------------------------|
| MESSAGE: ' . $values->message . ' 
|------------------------------------------------------------------------------|
| -- Datum odeslání: ' . $dat->format('c') . '
| -- IP odesilatele: ' . Ip::getRealIP() . '       
|------------------------------------------------------------------------------|
         ');

        /* try { */
        $model = new \Prihlaska\PrihlaskaModel($this->db);
        forEach ($values as $k => $v) {
            $fce = 'set' . ucfirst($k);
            $model->$fce($v);
        }
        $child_count = empty($values->child_count) ? 0 : $values->child_count;
        $model->setChild_count($child_count);
        $model->setIp(Ip::getRealIP());
        $model->setDate_registered($dat);
        $model->insert();
        /*    die('test');
          } catch (\Nette\InvalidArgumentException $e) {
          //throw new Exception($e);
          } catch (\Nette\Database\DriverException $e) {
          //throw new Exception($e);
          } catch (\Nette\Neon\Exception $e) {
          //throw new Exception($e);
          } */
        $mailer = new SendmailMailer();
        $mailer->send($mail);
        $this->flashMessage('Děkujeme za odeslání emailu.', 'success');
        $this->redirect('default');
    }

}
