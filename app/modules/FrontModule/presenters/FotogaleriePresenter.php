<?php

 /*
  * Pharo
  */

 namespace App\FrontModule\Presenters;

 /**
  * Description of FotogaleriePresenter
  *
  * @author hippo
  */
 class FotogaleriePresenter extends BasePresenter {
     
     
     public function renderDefault() {
         $gal = new \Common\Photogallery\PhotoGalleryModel();
         $photos = $gal->getFolder(__GAL_PATH__ . '2016/Pochod');
         $this->template->photos = $photos;
        //dump ( $photos );
     }
     
 }
 