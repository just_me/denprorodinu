<?php

/*
 * Pharo
 */

namespace App\FrontModule\Presenters;

/**
 * Description of SupportPresenter
 *
 * @author hippo
 */
class PodporaPresenter extends BasePresenter
{

    public function renderDefault()
    {

        $this->template->main_partners = $this->getMainPartners();
        $this->template->footer_partners = $this->getPartners(true);
    }

    public function renderLast()
    {
        $this->redirect('Homepage:default');
        $this->template->footer_partners = $this->getPartners(true,
                                                              '/www/partner/2016');
    }

}
