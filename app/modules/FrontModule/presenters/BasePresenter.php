<?php

namespace App\FrontModule\Presenters;

use Nette;
use Nette\Utils\Finder;
use Nette\Http\Response;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

    public $t_date = '2017-08-12 14:00:00';
    public $base_title = 'Den pro rodinu - Praha  12. 8. 2017';
    public $local_title = false;
    public $current_date;
    public $db;
    public $httpResponse;
    public $httpRequest;
    public $texty;

    public function __construct(Nette\Database\Context $database)
    {
        $this->db = $database;
        parent::__construct();
        $httpResponse = new Response();
        $httpResponse->addHeader('Pragma', 'no-cache');
        $this->httpResponse = $httpResponse;
    }

    public function startup()
    {
        parent::startup();
        //$this->template->footer_partners = $this->getPartners();
        $this->t_date = \Nette\Utils\DateTime::createFromFormat('Y-m-d H:i:s',
                                                                $this->t_date);
        $this->template->t_date = $this->t_date;
        $this->current_date = new \Nette\Utils\DateTime();
        \Common\Session::createCurrentSession();
    }

    public function beforeRender()
    {
        parent::beforeRender();
        $module = strtolower($this->getPureName());
        $result = $this->db->table('texty')
                ->select('s_name,s_content')
                ->where(['presenter' => $module])
                ->where(['s_version' => 1])
                ->fetchAll();
        $texty = [];
        if (!empty($result)) {
            forEach ($result as $row) {
                $texty[$row['s_name']] = $row['s_content'];
            }
        }
        $this->texty = $texty;
    }

    public function getPartners($exclude_main = false,
            $path = '/www/partner/footer')
    {
        $files = [];
        foreach (Finder::findFiles('*')->in(MAIN_PATH . $path) as $key => $file) {
            if (preg_match('/-main-/', $file->getFilename())) {
                continue;
            }

            $name = str_ireplace('logo', '', $file->getFilename());
            $name = str_replace('_', '', $name);

            if ($exclude_main === true) {
                if ($name == 'aliance.jpg') {
                    continue;
                }
            }
            $files[$name] = $file;
        }
        ksort($files);
        return $files;
    }

    public function getMainPartners()
    {
        $path = '/www/partner/footer';
        $files = [];
        foreach (Finder::findFiles('*')->in(MAIN_PATH . $path) as $key => $file) {
            if (preg_match('/-main-/', $file->getFilename())) {

                $name = str_ireplace('logo', '', $file->getFilename());
                $name = str_replace('_', '', $name);
                if ($name == 'aliance.jpg') {
                    continue;
                }
                $files[$name] = $file;
            }
        }
        ksort($files); 
        return $files;
    }

    public function getPureName()
    {
        $pos = strrpos($this->getPresenter()->name, ':');
        if (is_int($pos)) {
            return substr($this->getPresenter()->name, $pos + 1);
        }

        return $this->getPresenter()->name;
    }

    public function makeActive($item)
    {
        $presenter = strtolower($this->getPureName());
        if (strtolower($item) == $presenter) {
            return ' active';
        }
        return '';
    }

    public function getBase_title()
    {
        return $this->base_title;
    }

    public function getLocal_title()
    {
        return $this->local_title;
    }

    public function setBase_title($base_title)
    {
        $this->base_title = $base_title;
        return $this;
    }

    public function setLocal_title($local_title)
    {
        $this->local_title = $local_title;
        return $this;
    }

    public function getTitle()
    {
        if ($this->getLocal_title() !== false) {
            return $this->getBase_title() . ' - ' . $this->getLocal_title();
        }
        return $this->getBase_title();
    }

    public function getText($s_name)
    {
        if (empty($this->texty)) {
            return '';
        }
        if (array_key_exists($s_name, $this->texty)) {

            $text = $this->texty[$s_name];
            if ($this->request->getParameter('highlight') !== null) {
                if ($this->request->getParameter('highlight') == $s_name) {
                    $text = sprintf('<div style="border:1px solid red;">%s</div>',
                                    $text);
                }
            }
            return $text;
        }
        return '';
        /* $result = $this->db->table('texty')
          ->select('s_content')
          ->where( ['s_name' => $s_name] )
          ->order('s_version DESC')
          ->limit(1)
          ->fetch();
          return $result->s_content ; */
    }

}
