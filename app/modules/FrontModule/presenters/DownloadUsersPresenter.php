<?php

/*
 * Pharo
 */

namespace App\FrontModule\Presenters;

/**
 * Description of DownloadUsersPresenter
 *
 * @author hippo
 */
class DownloadUsersPresenter extends BasePresenter
{

    const CSV_PASS = 'denprorodinu919';
    const CSV_SESSION = 'AUTH_SESS_CSV';

    public function renderDefault()
    {
        $this->checkLogin();
    }

    public function actionDownload($type)
    {
        $this->checkLogin();
        $csv_array = [];
        if ($type == 'contact') {
            $data = $this->db->table('kontakt')->fetchAll();
        } else {
            $data = $this->db->table('prihlaska')->where('c_type', $type)->fetchAll();
        }

        forEach ($data as $key => $row) {
            $csv_array[] = $row;
        }
        $response = new \Nette\Application\Responses\CsvResponse($csv_array,
                                                                 $type . '-data.csv');
        $response->setOutputCharset('windows-1250');
        $this->sendResponse($response);
        $this->terminate();
    }

    public function actionSign()
    {
        if (empty($_POST['csv_auth_pass']) === true || $this->doLogin() === false) {
            $_SESSION[self::CSV_SESSION] = null;
            unset($_SESSION[self::CSV_SESSION]);
            $this->flashMessage('Prihlaseni se nezdarilo', 'danger');
            $this->redirect('in');
        }
    }

    protected function doLogin()
    {
        if (isset($_POST['csv_auth_pass'])) {
            $pass = trim($_POST['csv_auth_pass']);
            if ($pass == self::CSV_PASS) {
                $this->flashMessage('Uspesne jste se prihlasil', 'success');
                $_SESSION[self::CSV_SESSION] = true;
                $this->redirect('default');
            }
        }
        return false;
    }

    protected function checkLogin()
    {
        if (!isset($_SESSION[self::CSV_SESSION]) || $_SESSION[self::CSV_SESSION] !== true) {
            $this->flashMessage('Prosím zadejte platné heslo');
            $this->redirect('in');
        }
    }

}
