<?php

/*
 * Pharo
 */

namespace App\FrontModule\Presenters;

use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Utils\Validators;
use Nette\Utils\DateTime;
use Common\Ip;

/**
 * Description of KontaktPresenter
 *
 * @author hippo
 */
class KontaktPresenter extends BasePresenter {

    protected function createComponentContactForm() {
        $form = new Form();
        $a = 0;
        $form->elementPrototype->addAttributes(array('class' => 'register-form'));
        $form->addText('name_lastname', 'Vaše jméno a příjmení')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Vaše jméno a příjmení')
                ->setAttribute('tabindex', $a++)
                ->setRequired(true);
        $form->addEmail('email', 'Emailová adresa')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Emailová adresa')
                ->setAttribute('tabindex', $a++)
                ->setRequired(true);
        $form->addText('subject', 'Předmět')
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Předmět')
                ->setAttribute('tabindex', $a++);
        $form->addTextArea('message', 'Nějaký vzkaz ?', $cols = null, $rows = 5)
                ->setAttribute('class', 'form-control')
                ->setAttribute('placeholder', 'Nějaký vzkaz ?')
                ->setAttribute('tabindex', $a++);
        $form->addSubmit('submit', 'Odeslat zprávu')
                ->setAttribute('class', 'btn btn-theme btn-block btn-md chmnow')
                ->setAttribute('tabindex', $a++)
                ->setRequired(true);
        $form->onSuccess[] = [
            $this,
            'cContactSuccessed'
        ];


        return $form;
    }

    public function cContactSuccessed(Form $form) {
        $values = $form->getValues();

        if (
                empty($values->name_lastname) ||
                empty($values->email) ||
                empty($values->message)
        ) {
            $this->flashMessage('Vyplňte prosím všechny povinně údaje.', 'error');
            $form->addError('Vyplňte prosím všechny povinně údaje.');
            $this->redirect('default');
        }
        if (Validators::isEmail($values->email) === false) {
            $this->flashMessage('Email musí být ve správném tvaru.', 'error');
            $form->addError('Email musí být ve správném tvaru.');
            $this->redirect('default');
        }
        $dat = new DateTime();
        $subject = $values->subject === null ? 'Kontakt z www.denprorodinu.eu' : 'Kontakt z www.denprorodinu.eu' . $values->subject;
        $mail = new Message();
        $mail->setEncoding('UTF-8');
        $mail->setFrom('web@erneks.cz');
        $mail->addTo('info@denprorodinu.eu');
        $mail->addCc('hippo@network.cz');
        $mail->addReplyTo($values->email);
        $mail->setSubject($subject);
        $mail->setBody('
|------------------------------------------------------------------------------|
| Email Kontakt denprorodinu.eu
|------------------------------------------------------------------------------|
| OD: ' . $values->name_lastname . ' 
| EMAIL: ' . $values->email . ' 
|------------------------------------------------------------------------------|
| MESSAGE: ' . $values->message . ' 
|------------------------------------------------------------------------------|
|------------------------------------------------------------------------------|
| -- Datum odeslání: ' . $dat->format('c') . '
| -- IP odesilatele: ' . Ip::getRealIP() . '       
|------------------------------------------------------------------------------|
         ');

        try {
            $model = new \Kontakt\KontaktModel($this->db);
            forEach ($values as $k => $v) {
                $fce = 'set' . ucfirst($k);
                $model->$fce($v);
            }
            $model->setIp(Ip::getRealIP());
            $model->setDate_added($dat);
            $model->insert();
        } catch (\Nette\InvalidArgumentException $e) {
            // dump($e);
        } catch (\Nette\Database\DriverException $e) {
            // dump($e);
        } catch (\Nette\Neon\Exception $e) {
            
        }
        $mailer = new SendmailMailer();
        $mailer->send($mail);
        $this->flashMessage('Děkujeme za odeslání emailu.', 'success');
        $this->redirect('default');
    }

}
