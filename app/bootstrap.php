<?php

require __DIR__ . '/../vendor/autoload.php';

define('MAIN_PATH', dirname(__DIR__));


$configurator = new Nette\Configurator;

$configurator->setDebugMode('92.62.228.33'); // enable for your remote IP

$configurator->enableTracy(__DIR__ . '/../log');

//$configurator->setDebugMode(false);
$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
        ->addDirectory(__DIR__)
        ->addDirectory(__DIR__ . '/../src')
        ->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

return $container;
