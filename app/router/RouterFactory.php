<?php

 namespace App;

 use Nette;
 use Nette\Application\Routers\RouteList;
 use Nette\Application\Routers\Route;

 class RouterFactory {

     use Nette\StaticClass;

     /**
      * @return Nette\Application\IRouter
      */
     public static function createRouter() {
         
         $router = new RouteList();

         $router[] = $AdminRouter = new RouteList('Admin');
         $AdminRouter[] = new Route('admin/<presenter>/<action>[/<id>]','Dashboard:default');
         
         $router[] = $FrontRouter = new RouteList('Front');
         $FrontRouter[] = new Route('<presenter>/<action>', 'Homepage:default');
         
         return $router;
     }

 }

 /*
 * $router[] = new Route('podpora/<action>', 'Podpora:default');
                $router[] = new Route('prohlaseni/<action>', 'Prohlaseni:default');
                $router[] = new Route('kontakt/<action>', 'Kontakt:default');
 */