<?php

 /*
  * Pharo
  */

 namespace Common\Photogallery;

 /**
  * Description of PhotoGalleryModel
  *
  * @author hippo
  */
 class PhotoGalleryModel {

     public function getFolder($folder) {
         $gal = [];
         forEach (\Nette\Utils\Finder::findFiles('*_small.jpg')->in($folder) as $key => $file) {
             $gal[pathinfo($key, PATHINFO_BASENAME)]['file'] = $file;
             $gal[pathinfo($key, PATHINFO_BASENAME)]['large'] = str_replace('_small', '_large', $file->getBasename() );
         }
         ksort($gal);
         return $gal;
     }

 }
 