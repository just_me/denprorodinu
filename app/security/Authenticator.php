<?php

/*
 * Pharo
 */

namespace App\Security;

use \Nette\Security as NS;

/**
 * Description of Authenticator
 *
 * @author hippo
 */
class Authenticator extends \Nette\Object implements NS\IAuthenticator
{
    const UNAME = 'denprorodinu';
    const UPASS = '321adminden123';
    
    public function authenticate(array $credentials)
    {
        list ($username, $password ) = $credentials;
        if ( $username !== self::UNAME || $password !== self::UPASS ) {
            throw new NS\AuthenticationException('Invalid password or Username.');
        }
        $identity = new NS\Identity(1, 'admin',[]);
        return $identity;
    }
}
