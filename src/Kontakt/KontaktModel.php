<?php

/*
 * Pharo
 */

namespace Kontakt;

/**
 * Description of KontaktModel
 *
 * @author hippo
 */
class KontaktModel {

    protected $db;
    public $id;
    public $name_lastname;
    public $email;
    public $message;
    public $subject;
    public $ip;
    public $date_added;
    public $data = array();
    protected $table = 'kontakt';

    public function __construct(\Nette\Database\Context $database) {
        $this->db = $database;
    }

    public function insert() {
        $this->db->table($this->table)->insert($this->getData());
    }

    public function getData() {
        return $this->data;
    }

    public function setName_lastname($name_lastname) {
        $this->data['name_lastname'] = $name_lastname;
        return $this;
    }

    public function setEmail($email) {
        $this->data['email'] = $email;
        return $this;
    }

    public function setMessage($message) {
        $this->data['message'] = $message;
        return $this;
    }

    public function setSubject($subject) {
        $this->data['subject'] = $subject;
        return $this;
    }

    public function setIp($ip) {
        $this->data['ip'] = $ip;
        return $this;
    }

    public function setDate_added($date_added) {
        $this->data['date_added'] = $date_added;
        return $this;
    }

}
