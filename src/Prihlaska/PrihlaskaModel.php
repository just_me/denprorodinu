<?php

/*
 * Pharo
 */

use Nette\Database\Context;

namespace Prihlaska;

/**
 * Description of PrihlaskaModel
 *
 * @author hippo
 */
class PrihlaskaModel {

    protected $db;
    public $id;
    public $name_lastname;
    public $c_type;
    public $email;
    public $child_count;
    public $message;
    public $ip;
    public $date_registered;
    public $data = array();
    protected $table = 'prihlaska';

    public function __construct(\Nette\Database\Context $database) {
        $this->db = $database;
    }
    
    
    
    public function insert() {
        $this->db->table($this->table)->insert($this->getData());
    }
    
    
    public function getData() {
        return $this->data;
    }

        
    public function setName_lastname($name_lastname) {
        $this->data['name_lastname'] = $name_lastname;
        return $this;
    }

    public function setEmail($email) {
        $this->data['email'] = $email;
        return $this;
    }

    public function setChild_count($child_count) {
        $this->data['child_count'] = $child_count;
        return $this;
    }

    public function setMessage($message) {
        $this->data['message'] = $message;
        return $this;
    }

    public function setIp($ip) {
        $this->data['ip'] = $ip;
        return $this;
    }

    public function setDate_registered($date_registered) {
        $this->data['date_registered'] = $date_registered;
        return $this;
    }

    public function setC_type($c_type)
    {
        $this->data['c_type'] = $c_type;
        return $this;
    }


    

}
