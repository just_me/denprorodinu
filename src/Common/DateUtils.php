<?php

/*
 * Pharo
 */

namespace Common;

/**
 * Description of DateUtils
 *
 * @author hippo
 */
class DateUtils {
    
    public static function getCurrentDay() {
        $date = new \Nette\Utils\DateTime();
        return $date->format('Y-m-d');
    }
    
    
}
