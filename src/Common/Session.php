<?php

/*
 * Pharo
 */

namespace Common;

/**
 * Description of Session
 *
 * @author hippo
 */
class Session {

    const SESSION_CURRENT_NAME = 'CURRENT';

    public static function createCurrentSession() {
        $_SESSION[self::SESSION_CURRENT_NAME] = isset($_SESSION[self::SESSION_CURRENT_NAME]) ? $_SESSION[self::SESSION_CURRENT_NAME] : uniqid(time());
        return $_SESSION[self::SESSION_CURRENT_NAME];
    }

    public static function getCurrentSession() {
        return self::createSessionCurrent();
    }

}
