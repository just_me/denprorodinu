<?php
namespace Common;
/**
 * Heplers
 * @author hippo <hippo@network.cz>
 * @version $version
 */


class Ip
{

    public static function address($inet_antoa = false)
    {
        $ip = '';
        if (! empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (! empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        
        if ($inet_antoa === true) {
            return ip2long($ip);
        }
        return $ip;
    }

    private static function extractIP($ip)
    {
        $b = preg_replace("/^([0-9]{1,3}\.){3,3}[0-9]{1,3}/", $ip, $array);
        if ($b)
            return $array;
        else
            return false;
    }

    /*
     * --------------------------------------------------
     * get_IP()
     * --------------------------------------------------
     */
    static public function get_IP()
    {
        global $REMOTE_HOST, $REMOTE_ADDR;
        
        if ($REMOTE_HOST) {
            $array = self::extractIP($REMOTE_HOST);
            if ($array && count($array) >= 1) {
                return $array[0]; // first IP in the list
            }
        }
        
        return $REMOTE_ADDR;
    }

    static public function getProxy()
    {}

    /*
     * --------------------------------------------------
     * get_real_IP()
     * get the real IP if hidden by proxy
     * --------------------------------------------------
     */
    static public function getRealIP()
    {
        // Just get the headers if we can or else use the SERVER global
        if (function_exists('apache_request_headers')) {
            $headers = apache_request_headers();
        } else {
            $headers = $_SERVER;
        }
        // Get the forwarded IP if it exists
        if (array_key_exists('X-Forwarded-For', $headers) && filter_var($headers['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            $the_ip = $headers['X-Forwarded-For'];
        } elseif (array_key_exists('HTTP_X_FORWARDED_FOR', $headers) && filter_var($headers['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            $the_ip = $headers['HTTP_X_FORWARDED_FOR'];
        } else {
            $the_ip = filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
        }
        return $the_ip;
    }
}