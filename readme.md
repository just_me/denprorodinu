Den pro rodinu
=============
Install 

Stahnout git do adresare s www rootem 

$> composer install
$> mkdir temp
$> mkdir log 
Adresare temp a musi byt zapisovatelna pro www server 

Adresare: 
www_root/
    app/
    src/
    temp/
    vendor/
    www/ - index.php



Hotovo 

Aplikace Nette 
    app/modules/FrontModule - soubory front endu 
    app/modules/FrontModule/HomepagePresenter.php - class pro zobrazeni homepage 
    app/Modules/FrontModule/templates/Homepage/default.latte - sablona pro Homepage
    app/Modules/FrontModule/templates/@layout.latte -  vychozi sablona 

    app/config/config.neon - YAML file pro konfiguraci 
    app/config/config.local.neon - YAML file pro overloading hodnot pro aplikaci

Muj lokalni config neon 

__BOF__

parameters:

database:
	dsn: 'mysql:host=127.0.0.1;dbname=denprorodinu'
	user: xxxxxxxx
	password: xxxxxxxxx
	options:
		lazy: yes






License
-------
- Nette: New BSD License or GPL 2.0 or 3.0
- Adminer: Apache License 2.0 or GPL 2
