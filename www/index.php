<?php

// Uncomment this line if you must temporarily take down your site for maintenance.
// require __DIR__ . '/.maintenance.php';


 require_once __DIR__ . '/../paths.php';

 $container = require __DIR__ . '/../app/bootstrap.php';

 $container->getByType(Nette\Application\Application::class)->run();
 